<?php

class Dieters_OneStepCheckout_Block_Fields extends Idev_OneStepCheckout_Block_Fields {

    /**
     * Get country radio html
     * 
     * @param string $type
     * @return string
     */
    public function getCountryRadio($type)
    {
        $websiteCode = Mage::app()->getWebsite()->getCode();
        if ($type == 'billing') {
            $address = $this->getQuote()->getBillingAddress();
        } else {
            $address = $this->getQuote()->getShippingAddress();
        }

        $countryId = $address->getCountryId();
        if (is_null($countryId)) {
            $countryId = Mage::getStoreConfig('general/country/default');
        }

        $options = $this->getCountryOptions();
        
        // Nederland should be first
        $options = array_reverse($options);

        return $this->getRadioListHtml($options, $countryId, $type);
    }

    /**
     * Build country radio html
     * 
     * @param array $options
     * @param string $countrySelected
     * @param string $type
     * @return boolean|string
     */
    public function getRadioListHtml($options, $countrySelected, $type)
    {


        $outputHtml = '';

        if (!empty($options)) {
            foreach ($options as $option) {
                if ($option['value'] == '')
                    continue;

                $outputHtml .= '<span><input type="radio" name="' . $type . '[radio_country_id]" value="' . $option['value'] . '" id="' . $type . ':country_id_' . $option['value'] . '"' . ( ($countrySelected == $option['value']) ? ' checked="checked"' : '' ) . '/>';
                $outputHtml .= '<label for="' . $type . ':country_id_' . $option['value'] . '">' . $option['label'] . '</label></span>' . "\n";
            }
        }

        return $outputHtml;
    }

}
