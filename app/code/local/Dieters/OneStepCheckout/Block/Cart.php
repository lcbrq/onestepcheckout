<?php

class Dieters_OneStepCheckout_Block_Cart extends Idev_OneStepCheckout_Block_Checkout_Cart {
    
    /**
     * Get shipping cost
     * 
     * @return float
     */
    public function getShippingCost(){
        return Mage::getModel("checkout/session")->getQuote()->getShippingAddress()->getShippingAmount();
    }
    
}
