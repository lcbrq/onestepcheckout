<?php
class Dieters_OneStepCheckout_Model_Sales_Quote_Address_Total_Shipping extends Mage_Sales_Model_Quote_Address_Total_Shipping
{
    /**
     * Class contructor
     */
    public function __construct(){

    }

    /**
     * Add shipping totals information to address object - Rewrite
     * @author Jigsaw Marcin Gierus <martin@lcbrq.com>
     * @see Mage_Sales_Model_Quote_Address_Total_Shipping@fetch  
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_Sales_Model_Quote_Address_Total_Shipping
     */
        public function fetch(Mage_Sales_Model_Quote_Address $address)
        {
            $amount = $address->getShippingAmount();
            if ($amount != 0 || $address->getShippingDescription()) {
                $title = Mage::helper('sales')->__('Transportkosten');
                
                $address->addTotal(array(
                                         'code' => $this->getCode(),
                                         'title' => $title,
                                         'value' => $address->getShippingAmount()
                                         ));
            }
            return $this;
        }
    }

