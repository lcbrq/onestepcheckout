<?php

/**
 * Setup checkout fields
 */
$installer = $this;
$installer->startSetup();

$config = new Mage_Core_Model_Config();

$config->saveConfig('general/country/default', "NL");
$config->saveConfig('general/country/allow', "BE,NL");

$config->saveConfig('onestepcheckout/exclude_fields/exclude_region', "0");
$config->saveConfig('onestepcheckout/exclude_fields/exclude_telephone', "0");
$config->saveConfig('onestepcheckout/exclude_fields/exclude_fax', "1");

$config->saveConfig('onestepcheckout/sortordering_fields/company', 10);
$config->saveConfig('onestepcheckout/sortordering_fields/firstname', 20);
$config->saveConfig('onestepcheckout/sortordering_fields/lastname', 30);
$config->saveConfig('onestepcheckout/sortordering_fields/country_id', 40);
$config->saveConfig('onestepcheckout/sortordering_fields/postcode', 50);
$config->saveConfig('onestepcheckout/sortordering_fields/street', 60);
$config->saveConfig('onestepcheckout/sortordering_fields/city', 70);
$config->saveConfig('onestepcheckout/sortordering_fields/telephone', 80);
$config->saveConfig('onestepcheckout/sortordering_fields/region_id', 90);
$config->saveConfig('onestepcheckout/sortordering_fields/email', 100);
$config->saveConfig('onestepcheckout/sortordering_fields/fax', 110);
$config->saveConfig('onestepcheckout/sortordering_fields/taxvat', 120);
$config->saveConfig('onestepcheckout/sortordering_fields/dob', 130);
$config->saveConfig('onestepcheckout/sortordering_fields/gender', 140);
$config->saveConfig('onestepcheckout/sortordering_fields/create_account', 150);
$config->saveConfig('onestepcheckout/sortordering_fields/password', 160);
$config->saveConfig('onestepcheckout/sortordering_fields/confirm_password', 170);
$config->saveConfig('onestepcheckout/sortordering_fields/save_in_address_book', 180);

$config->saveConfig('postcodenl_api/advanced_config/never_hide_country', 1);
$config->saveConfig('customer/address/taxvat_show', 1);
