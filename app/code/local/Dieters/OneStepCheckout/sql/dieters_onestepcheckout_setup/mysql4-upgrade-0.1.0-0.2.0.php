<?php

$installer = $this;
$installer->startSetup();

$config = new Mage_Core_Model_Config();

$config->saveConfig('onestepcheckout/general/default_country', 'NL');
$config->saveConfig('onestepcheckout/general/rewrite_checkout_links', '1');

$installer->endSetup();
